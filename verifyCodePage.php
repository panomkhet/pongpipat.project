<div class="card-content">
    <span class="card-title center-align grey-darken-2-text">ยืนยันรหัสกิจกรรม</span>

    <form>
            <div class="input-field">
                <i class="material-icons prefix grey-text">account_circle</i>
                    <input id="place" type="text" class="validate">
                <label for="place">รหัสกิจกรรม</label>
            </div>

            <div class="center-align">
                <button class="btn red waves-effect waves-light">ยืนยัน
                <i class="material-icons right">send</i></button>
            </div>

    </form>
</div>