<div class="card-content">
    <span class="card-title center-align grey-darken-2-text">ลงทะเบียนผู้ใช้ใหม่</span>

    <form>
            <div class="input-field">
                <i class="material-icons prefix grey-text">account_circle</i>
                    <input id="place" type="text" class="validate">
                <label for="place">รหัสนักศึกษา</label>
            </div>

            <div class="input-field">
                <i class="material-icons prefix grey-text">account_circle</i>
                    <input id="place2" type="text" class="validate">
                <label for="place2">ชื่อสกุล</label>
            </div>

            <div class="input-field">
                <i class="material-icons prefix grey-text">account_circle</i>
                    <input id="place3" type="text" class="validate">
                <label for="place3">คณะ</label>
            </div>

            <div class="center-align">
                <button class="btn red waves-effect waves-light">Sign Up
                <i class="material-icons right">send</i></button>
            </div>

    </form>
</div>